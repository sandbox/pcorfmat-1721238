<?php
/**
 * @file
 * Admin forms for ViewCRUD.
 */

 /**
  * Admin settings form.
  */
function viewcrud_admin_settings() {

  $form['#prefix'] = t('<p>View CRUD settings.</p>');

  /* Get the content types list and add a checkbox list to the form */
  $node_types = node_type_get_names();
  $form['node_types']['nodes_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled content types'),
    '#description' => t('ViewCRUD allows you to enable the viewcrud components for any content type.  Choose the types on which you would like to associate viewcrud components.'),
    '#options' => $node_types,
    '#default_value' => viewcrud_variable_get('viewcrud_node_types'),
  );

  /* Get the list of the menus and add a selection list with a "None" choice */
  $menus = array('_none' => t('- None -'));
  $menus = array_merge($menus, menu_get_menus());
  $parent_menu = viewcrud_variable_get('viewcrud_menu');
  $form['menus'] = array(
    '#type' => 'select',
    '#title' => t('Menus'),
    '#description' => t('Choose a menu for the "Site management" item. If you choose "- None", you can create your own menu item with the link "viewcrud"'),
    '#options' => $menus,
    '#default_value' => $parent_menu ,
  );

  $form = system_settings_form($form);
  array_unshift($form['#submit'], 'viewcrud_admin_settings_submit');

  return $form;
}

/**
 * Submit handler for the viewcrud_admin_settings() form.
 */
function viewcrud_admin_settings_submit($form, &$form_state) {

  $query = db_select('views_view', 'i')->fields('i');
  $query->join('views_display', 't', 'i.vid = t.vid');
  $query->condition('t.display_plugin', 'page', '=');
  $result = $query->execute();

  /* Get a array of all the path of views including 'view_crud' */
  $path_array = array();
  while ($record = $result->fetchAssoc()) {
    $view = views_get_view($record['name']);
    if (isset ($view->display)) {
      foreach ($view->display as $display) {
        if (isset($display->display_options['path'])) {
          $path = $display->display_options['path'];
          if (strpos($path, 'view_crud') !== FALSE) {
            $path_array[] = $path;
          }
        }
      }
    }
  }

  /* For each content types selected, a view is generated
   * If a view with a path including 'view_crud' already exist, no generation
   * will be processed in this case, the existing view will be used with the
   * module.
   */
  $type_array = array();
  foreach ($form_state['input']['nodes_types'] as $key => $type) {
    if (!empty ($type)) {
      /* This content type is selected */
      $type_array[] = $type;
      $path = 'view_crud/' . $type . '/list';
      /* Is a view with a path including 'view_crud' exist ? */
      if (in_array($path, $path_array)) {
        /* The name of the generated views are 'view_crud_" followed with the
         * content type name.
         * The existing view is loaded and the menu type is save with
         * 'default tab' value.
         */
        $view = views_get_view('view_crud_' . $type);
        if (isset ($view->display[$type]->display_options['menu']['type'])) {
          $view->display[$type]->display_options['menu']['type'] = 'default tab';
          $view->save();
        }
        $mess = 'A display page for the view ' . $view->human_name . ' already exists with the path '
                . $path . '. The CRUD view is not created. The display page will be used for this view.';
        drupal_set_message(check_plain($mess));
      }
      else {
        /* The view does not exist : It's created */
        $view = viewcrud_create_view($type);
        $mess = 'A display page with the path ' . $path . ' was created for the view ' . $view->human_name;
        drupal_set_message(check_plain($mess));
      }
    }
    else {
      /* This content type is not selected
       * If a generated view already exist, it is not deleted not the tabs are suppressed */
      $view = views_get_view('view_crud_' . $key);
      if (isset($view->display[$key]->display_options['menu']['type'])) {
        $view->display[$key]->display_options['menu']['type'] = 'none';
        $view->save();
      }
    }
  }
  variable_set('viewcrud_node_types', $type_array);
  variable_set('viewcrud_menu', $form_state['input']['menus']);
  menu_rebuild();
}

/**
 * Create a viewCRUD. Called when the admin setting form is submitted.
 */
function viewcrud_create_view($type) {
  $types = _node_types_build();
  $type_name = $types->names[$type];

  $view = new view();
  $view->name = 'view_crud_' . $type;
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'View CRUD ' . $type_name;
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = $type_name . ' List';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['label'] = '';
  $handler->display->display_options['fields']['edit_node']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['edit_node']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['edit_node']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['edit_node']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['edit_node']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['edit_node']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['edit_node']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['edit_node']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['edit_node']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['edit_node']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['edit_node']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['edit_node']['alter']['html'] = 0;
  $handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['edit_node']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['edit_node']['hide_empty'] = 0;
  $handler->display->display_options['fields']['edit_node']['empty_zero'] = 0;
  $handler->display->display_options['fields']['edit_node']['hide_alter_empty'] = 1;
  /* Field: Content: Delete link */
  $handler->display->display_options['fields']['delete_node']['id'] = 'delete_node';
  $handler->display->display_options['fields']['delete_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['delete_node']['field'] = 'delete_node';
  $handler->display->display_options['fields']['delete_node']['label'] = '';
  $handler->display->display_options['fields']['delete_node']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['delete_node']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['delete_node']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['delete_node']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['delete_node']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['delete_node']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['delete_node']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['delete_node']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['delete_node']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['delete_node']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['delete_node']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['delete_node']['alter']['html'] = 0;
  $handler->display->display_options['fields']['delete_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['delete_node']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['delete_node']['hide_empty'] = 0;
  $handler->display->display_options['fields']['delete_node']['empty_zero'] = 0;
  $handler->display->display_options['fields']['delete_node']['hide_alter_empty'] = 1;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    $type => $type,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'CRUD View', $type);
  $handler->display->display_options['path'] = 'view_crud/' . $type . '/list';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'List';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['tab_options']['type'] = 'normal';
  $handler->display->display_options['tab_options']['title'] = $type_name;
  $handler->display->display_options['tab_options']['description'] = $type_name . ' management';
  $handler->display->display_options['tab_options']['weight'] = '0';

  $view->save();
  return $view;
}
