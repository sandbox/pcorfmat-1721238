<?php

/**
 * @file
 * ViewCRUD module main routines.
 */

require_once dirname(__FILE__) . '/viewcrud.admin.inc';

/**
 * Get variable for the module.
 */
function viewcrud_variable_get($variable) {
  switch ($variable) {
    case 'viewcrud_node_types':
      $result = variable_get('viewcrud_node_types', array());
      break;

    case 'viewcrud_menu':
      $result = variable_get('viewcrud_menu', 'navigation');
      break;
  }
  return $result;
}


/**
 * Update the variable 'viewcrud_node_types'.
 */
function viewcrud_node_type($op, $info) {
  $viewcrud_types = viewcrud_variable_get('viewcrud_node_types');
  $affected_type = isset($info->old_type) ? $info->old_type : $info->type;
  $key = array_search($affected_type, $viewcrud_types);
  if ($key !== FALSE) {
    if ($op == 'update') {
      $viewcrud_types[$key] = $info->type;
    }
    if ($op == 'delete') {
      unset($viewcrud_types[$key]);
    }
    variable_set('viewcrud_node_types', $viewcrud_types);
  }
}

/**
 * Implements hook_node_type_update().
 */
function viewcrud_node_type_update($info) {
  viewcrud_node_type('update', $info);
}

/**
 * Implements hook_node_type_delete().
 */
function viewcrud_node_type_delete($info) {
  viewcrud_node_type('delete', $info);
}

/**
 * Page callback for the tab 'List' in the view.
 */
function viewcrud_list() {
  drupal_goto('view_crud/' . arg(1) . '/list');
}

/**
 * Page callback for the tab 'List' in the node form.
 */
function viewcrud_node_add_list() {
  drupal_goto('view_crud/' . arg(2) . '/list');
}

/**
 * Page callback for the tab 'Add' in the view.
 */
function viewcrud_add() {
  drupal_goto('node/add/' . arg(1));
}

/**
 * Page callback for the tab 'Add' in the node form.
 */
function viewcrud_node_add_add() {
  drupal_goto('node/add/' . arg(2));
}

/**
 * Page callback for the tab 'List' in the node form.
 */
function viewcrud_node_list() {
  $node = node_load(arg(1));
  drupal_goto('view_crud/' . $node->type . '/list');
}

/**
 * Page callback for the tab 'Add' in the node form.
 */
function viewcrud_node_add() {
  $node = node_load(arg(1));
  drupal_goto('node/add/' . $node->type);
}

/**
 * Implements hook_menu_link_alter().
 */
function viewcrud_menu_link_alter(&$item) {
  $types = viewcrud_variable_get('viewcrud_node_types');
  foreach ($types as $type) {
    if ($item['link_path'] == 'view_crud/' . $type) {
      $item['hidden'] = 1;
    }
  }
}

/**
 * Implements hook_menu().
 */
function viewcrud_menu() {

  $items = array();

  $types = viewcrud_variable_get('viewcrud_node_types');
  /* Site management menu */
  $menu = viewcrud_variable_get('viewcrud_menu');
  if ($menu != '_none') {
    $items['viewcrud'] = array(
      'title' => "Website management",
      'description' => "Website management.",
      'type' => MENU_NORMAL_ITEM,
      'page callback' => 'system_admin_menu_block_page',
      'access callback' => TRUE,
      'file' => 'system.admin.inc',
      'file path' => drupal_get_path('module', 'system'),
      'menu_name' => $menu,
    );
  }

  $node_types = node_type_get_names();
  /* Menu items tabs "List" and "Add" in the view for each content type */
  foreach ($types as $type) {
    if ($menu != '_none') {
      $items['viewcrud/' . $type . '/list'] = array(
        'type' => MENU_NORMAL_ITEM,
        'description' => $node_types[$type] . ' management',
        'title' => $node_types[$type],
        'page callback' => 'viewcrud_list',
        'access callback' => TRUE,
        'weight' => 10,
        'menu_name' => $menu,
      );
      $items['view_crud/' . $type . '/list'] = array(
        'type' => MENU_LOCAL_TASK,
        'title' => 'List',
        'page arguments'   => array(1, 'node'),
        'page callback' => 'viewcrud_list',
        'access callback' => TRUE,
        'weight' => 10,
      );
      $items['view_crud/' . $type . '/add'] = array(
        'type' => MENU_LOCAL_TASK,
        'title' => 'Add',
        'page arguments'   => array(1, 'node'),
        'page callback' => 'viewcrud_add',
        'access callback' => TRUE,
        'weight' => 20,
      );
    }
    $items['node/add/' . $type . '/list'] = array(
      'type' => MENU_LOCAL_TASK,
      'title' => 'List',
      'page arguments'   => array(1, 'node'),
      'page callback' => 'viewcrud_node_add_list',
      'access callback' => TRUE,
      'weight' => 10,
    );

    $items['node/add/' . $type . '/add'] = array(
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'title' => 'Add',
      'page arguments'   => array(1, 'node'),
      'page callback' => 'viewcrud_node_add_add',
      'access callback' => TRUE,
      'weight' => 20,
    );
  }

  /* Menu items tabs "List" and "Add" in the node form */
  $items['node/%node/list'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => 'List',
    'page arguments'   => array(1, 'node'),
    'page callback' => 'viewcrud_node_list',
    'access arguments' => array(1, 'node'),
    'access callback' => TRUE,
    'weight' => -20,
  );
  $items['node/%node/add'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => 'Add',
    'page arguments'   => array(1, 'node'),
    'page callback' => 'viewcrud_node_add',
    'access arguments' => array(1, 'node'),
    'access callback' => TRUE,
    'weight' => -10,
  );
  /* Admin menu for the module */
  $items['admin/config/viewcrud'] = array(
    'title' => 'ViewCRUD settings',
    'description' => 'Global configuration of viewcrud functionality.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('viewcrud_admin_settings'),
    'access callback' => 'user_access',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}
