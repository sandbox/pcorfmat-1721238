viewCRUD Module
------------------------
by Philippe Corfmat, pcorfmat@sudoware.fr

Description
-----------
This module allows you to add a item menu in a menu to manage your node by
content type. It offers a useful UI for the end user to manage your content
without using the administration UI of the core.
For each content type you select in the configuration of this module, a view
is generated. In this view : Title, edit link and delete link are displayed.
In addition, a "Add" tab allows to to a create a new content.
In the creation form of the node, a "List" tab allow you to come back to the
view.

<DRUPAL 7>
Installation
------------
Simply go to the module administration
screen and select the module and click Save.

How to use it
-------------
Go to the module administration screen and click Configure for the viewCRUD
module.
Then choose the content types. For each selected content types a view is
generated.
